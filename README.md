# Install Flutter Tutorial

### Requirements
  - Operating Systems: Windows 7 SP1 or later (64-bit)
  - Disk Space: 400 MB (tidak termasuk IDEa dan tools lain).
  - Tools: Flutter bergantung pada tools berikut
    - Windows PowerShell 5.0 or newer (this is pre-installed with Windows 10)
    - Git for Windows 2.x, with the Use Git from the Windows Command Prompt 
    > jika sudah menginstall git, bisa di cek dengan menggunakan comand
    > pada cmd `git` jika not found, coba install ulang
___
### Another Requirements
  - Sudah install JDK (Java Development Kit)
  - Sudah install Android SDK (Software Development Kit)
> ke-2 requirement diatas tidak akan dibahas disini karena disini hanya membahas instalasi flutter
----
### Step by Step Install SDK (Windows):
  1. flutter SDK bisa di download dengan 2 cara:
     - download zip nya langsung [disini](https://flutter.dev/docs/get-started/install/windows#get-the-flutter-sdk)
     - atau pake git: `git clone https://github.com/flutter/flutter.git`
  2. letakkan Flutter SDK yang sudah didownload di tempat terserah (jangan taruh di C:/Program Files karena memerlukan permission kusus setiap diakses, karena agak ribet dengan permission admin)
  3. buka directory nya, lalu buka `%directory_anda%/flutter/bin`, dan copy full path nya *contoh: D:\Andrew\flutter\bin*
  4. lalu buka `Control Panel` > `System and Security` > `System` > `Advanced system settings(biasanya layar bagian kiri)` > `Environment Variables`
  5. pada bagian `system variables` tambahkan baru (New...) **paste** full path yang tadi dicopy (*contoh D:\Andrew\flutter\bin*)
___
### Step by Step Install SDK (Linux):
  1. flutter SDK bisa di download dengan 2 cara:
     - download zip nya langsung [disini](https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.12.13+hotfix.5-stable.tar.xz)
     - atau pake git: `git clone https://github.com/flutter/flutter.git`
  2. letakkan Flutter SDK yang sudah didownload terserah dimana aja
  3. buka directory nya, lalu buka `%directory_anda%/flutter/bin`, dan copy full path nya *contoh: /home/angdrew/Documents/sdk/flutter/bin/flutter* atau singkat nya *$HOME/Documents/sdk/flutter/bin*
  4. lalu buka terminal dan run jalankan: `nano ~/.bashrc`, tambahkan di paling bawah directory export path beserta directory anda seperti ini: `export PATH="$PATH:$HOME/Documents/sdk/flutter/bin"` *catatan penting: directory kita mungkin tidak sama, jangan di tiru 100% mohon pahami sendiri maksud nya seperti bagaimana
  5. ctrl + x, y, enter. jalankan `source ~/.bashrc` untuk mereload
___

### Step by Step Install Plugin

####  dengan Visual Studio Code (*Recomended*)
  1. buka extensions manager di sebelah kiri layar
  2. cari `Flutter` dan install, lalu oke (nanti otomatis akan install `Dart` juga by default)
#### dengan Android Studio
  1. buka settings, search Plugins
  2. cari `Flutter` di marketplace dan lalu klik install
> kenapa VSCode lebih recomended?
> karena saya rasa VSCode lebih responsif, dan lebih mudah digunakan daripada android studio dan juga lebih ringan untuk running, nggak kalah powerful nya sama IDEa seberat Android Studio 
> catatan: install keduanya lebih bagus, karena ada hal yang tidak bisa dilakukan di VSCode tapi bisa di Android Studio seperti upgrade gradle project atau refactoring / migrating. *install plugin nya di salah satu saja (yang kita pakai untuk editing)
___
### Setup Device
#### Physical Device
  1. Siapkan device android
  2. buka about phone, klik build number 8x untuk mengaktifkan **Developer Mode / Mode Pengembang** (ada beberapa merek yang berbeda cara mengaktifkan nya, silahkan google dengan keyword: `how to enable developer mode on [merek_hp_anda]`)
  3. buka `Developer Options` atau `Additional Setting`> `Developer Options`
  4. nyalakan `USB Debugging` (dan `allow install from USB` jika ada *biasanya hp xiaomi yang ada*)
  5. lalu sambungkan perangkat anda dengan kabel USB ke komputer, dan hp anda sudah siap untuk debugging
#### Virtual Device
  1. buka android studio
  2. klik AVD Manager
  3. create virtual device
  4. pilih tipe yang anda inginkan, klik next
  5. pilih system image nya, klik next
  6. ubah nama jika perlu, lalu Finish
> catatan download system image nya kira" ngabisin quota 1.5 GBan
___
**Done!!!** Flutter is ready to use~!!

try to run your first project with `flutter run` command on `terminal window` at VSCode screen (if not appear press CTRL + `)

note: it takes longer for the first run, and it depends on your internet
